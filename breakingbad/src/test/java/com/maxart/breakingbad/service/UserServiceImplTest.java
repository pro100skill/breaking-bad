package com.maxart.breakingbad.service;

import com.maxart.breakingbad.model.Stats;
import com.maxart.breakingbad.model.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@Transactional
class UserServiceImplTest {

    @Autowired
    private UserService sut;

    private static User newCustomer;

    @BeforeAll
    static void setUp() {
        newCustomer = User.builder()
                .password("testCustomer")
                .username("testCustomer")
                .code("777")
                .build();
    }

    @Test
    void save() {
        sut.save(newCustomer);

        assertNotNull(newCustomer.getRole());
        assertEquals(newCustomer.getRole().getName(), "ROLE_CUSTOMER");
    }

    @Test
    void findByUsername() {
        User user = sut.findByUsername("gustavo").orElse(null);

        assertNotNull(user);
        assertEquals(user.getId().intValue(), 1);
        assertEquals(user.getRole().getName(), "ROLE_CARTEL");
    }

    @Test
    void findById() {
        User user = sut.findById(1L).orElse(null);

        assertNotNull(user);
        assertEquals(user.getUsername(), "gustavo");
        assertEquals(user.getRole().getName(), "ROLE_CARTEL");
    }

    @Test
    void findByRole() {
        List<String> expected = Arrays.asList("gustavo", "support");

        List<User> users = sut.findByRole(6L);
        List<String> names = new ArrayList<>();
        for (User user: users) {
            names.add(user.getUsername());
        }

        assertTrue(expected.equals(names));
    }

    @Test
    void getStats() throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date st = formatter.parse("2018-06-01");
        Date fn = formatter.parse("2018-06-08");
        List<Stats> stats = sut.getStats(st, fn);

        assertEquals(stats.size(), 7);
        double inc = 0.0, out = 0.0;
        for (Stats stat: stats) {
            inc+=stat.getIncoming();
            out+=stat.getOutgoing();
        }

        assertEquals(inc, 117000);
        assertEquals(out, 42640);
    }
}