package com.maxart.breakingbad.service;

import com.maxart.breakingbad.model.Message;
import com.maxart.breakingbad.model.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@Transactional
class MessageServiceImplTest {

    @Autowired
    private MessageService sut;

    @Autowired
    private UserService userService;

    @Test
    void save() {
        User gustavo = userService.findByUsername("gustavo").orElse(null);

        Message msg = Message.builder()
                .recipient(gustavo)
                .text("Hello, how a u?")
                .build();

        sut.save(gustavo, msg);
    }

    @Test
    void findById() {
        Message msg = sut.findById(1L).orElse(null);
        assertNull(msg);
    }

    @Test
    void findAll() {
        User gustavo = userService.findByUsername("gustavo").orElse(null);

        Page<Message> messages = sut.findAll(gustavo, "in", 5, 5);
        assertEquals(messages.getTotalPages(), 0);
        assertEquals(messages.getTotalElements(), 0);

        messages = sut.findAll(gustavo, "out", 5, 5);
        assertEquals(messages.getTotalPages(), 0);
        assertEquals(messages.getTotalElements(), 0);
    }

    @Test
    void deleteById() {
        User gustavo = userService.findByUsername("gustavo").orElse(null);

        assertEquals(sut.deleteById(gustavo, 3L), "error");
    }
}