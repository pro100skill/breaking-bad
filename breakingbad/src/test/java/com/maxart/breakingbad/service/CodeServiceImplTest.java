package com.maxart.breakingbad.service;

import com.maxart.breakingbad.model.Code;
import com.maxart.breakingbad.model.Role;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@Transactional
class CodeServiceImplTest {

    @Autowired
    private CodeService sut;

    @Test
    void findPaginated() {
        Page<Code> codes = sut.findPaginated(5, 5);
        assertEquals(codes.getTotalPages(), 0);
        assertEquals(codes.getTotalElements(), 0);
    }

    @Test
    void save() {
        Date now = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(now);
        c.add(Calendar.DATE, 10);
        Date expiresAt = c.getTime();

        Role role = Role.builder().id(3L).name("ROLE_COOK").build();
        Code code = Code.builder()
                .role(role)
                .expiresAt(expiresAt)
                .build();
        sut.save(code);
    }

    @Test
    void findById() {
        Code code = sut.findById(999L).orElse(null);
        assertNull(code);
    }
}