package com.maxart.breakingbad.service;

import com.maxart.breakingbad.model.Ingredient;
import com.maxart.breakingbad.model.Report;
import com.maxart.breakingbad.model.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@Transactional
class ReportServiceImplTest {

    @Autowired
    private ReportService sut;

    @Autowired
    private UserService userService;

    @Test
    void create() {
        User provider = userService.findByUsername("provider").orElse(null);
        assertNotNull(provider);
        List<Ingredient> ingredients = Arrays.asList(
                Ingredient.builder().ingredientName("Ingredient1").ingredientPrice(33).ingredientAmount(0).build(),
                Ingredient.builder().ingredientName("Ingredient1").ingredientPrice(33).ingredientAmount(0).build(),
                Ingredient.builder().ingredientName("Ingredient1").ingredientPrice(34).ingredientAmount(0).build());

        Report report = Report.builder()
                .reportType("CASH")
                .ingredient(ingredients)
                .total(100.00)
                .build();

        sut.create(provider, report);

        assertNotNull(report.getId());
        assertEquals(report.getUser().getUsername(), provider.getUsername());
    }
}