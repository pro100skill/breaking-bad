package com.maxart.breakingbad.service;

import com.maxart.breakingbad.model.Order;
import com.maxart.breakingbad.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@Transactional
class OrderServiceImplTest {

    @Autowired
    private OrderService sut;

    @Autowired
    private UserService userService;

    @Test
    void create() {
        User customer = userService.findByUsername("customer").orElse(null);

        Order order = Order.builder()
                .customer(customer)
                .city("Moscow")
                .district("District")
                .quantity(10)
                .build();

        sut.create(customer, order);

        assertNotNull(order.getId());
        assertNotNull(order.getCreatedAt());
        assertEquals(order.getStatus(), "NEW");
        assertEquals(order.getPaymentStatus(), "UNPAID");
        assertEquals(order.getQuantity(),10);
        assertEquals(order.getAmount(),10 * 20);
    }

    @Test
    void appointDealer() {
        User dealer = userService.findByUsername("dealer").orElse(null);

        Order order = sut.findById(2L).orElse(null);
        assertNotNull(order);
        order.setDealer(dealer);

        sut.appointDealer(dealer, order);

        assertEquals(order.getDealer().getUsername(), "dealer");
        assertEquals(order.getStatus(), "WAITING");
    }

    @Test
    void acceptOrder() {
        Order order = sut.findById(1L).orElse(null);
        assertNotNull(order);

        sut.acceptOrder(new Date(), "place", "photo", order);

        assertNotNull(order.getDeliveryAt());
        assertEquals(order.getPlace(), "place");
        assertEquals(order.getPhoto(), "photo");
        assertEquals(order.getStatus(), "EXECUTABLE");
    }

    @Test
    void completeOrder() {
        Order order = sut.findById(5L).orElse(null);
        assertNotNull(order);
        sut.completeOrder(order);

        assertEquals(order.getStatus(), "COMPLETED");
    }

    @Test
    void payOrder() {
        Order order = sut.findById(5L).orElse(null);
        assertNotNull(order);

        sut.payOrder("CANCELED", order);
        assertEquals(order.getStatus(), "CANCELED");

        sut.payOrder("PAID", order);
        assertEquals(order.getPaymentStatus(), "PAID");
    }

    @Test
    void findById() {
        Order order = sut.findById(1L).orElse(null);

        assertNotNull(order);

        assertEquals(order.getQuantity(), 15);
        assertEquals(order.getStatus(), "WAITING");
        assertNotNull(order.getDealer());
        assertEquals(order.getAmount(), 300.0);
    }

    @Test
    void find() {
        User gustavo = userService.findByUsername("gustavo").orElse(null);
        Page<Order> orders = sut.find(gustavo, "NEW", 0, 3);

        assertEquals(orders.getTotalPages(), 2);
        assertEquals(orders.getTotalElements(), 5);

        Order order = orders.iterator().next();
        assertEquals(order.getCity(), "Moscow");
        assertEquals(order.getDistrict(), "Где я?");
        assertEquals(order.getStatus(), "NEW");
        assertEquals(order.getQuantity(), 160);
        assertEquals(order.getAmount(), order.getQuantity() * 20);
        assertEquals(order.getPaymentStatus(), "UNPAID");
    }
}