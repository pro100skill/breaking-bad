package com.maxart.breakingbad.service;

import com.maxart.breakingbad.model.Proposal;
import com.maxart.breakingbad.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@Transactional
class ProposalServiceImplTest {

    @Autowired
    private ProposalService sut;

    @Autowired
    private UserService userService;

    @Test
    void findById() {
        Proposal proposal = sut.findById(1L).orElse(null);

        assertNotNull(proposal);

        assertEquals(proposal.getProposalType(), "INGREDIENT");
        assertEquals(proposal.getStatus(), "NEW");
        assertEquals(proposal.getQuantity(), 0);
    }

    @Test
    void find() {
        User walter = userService.findByUsername("walter").orElse(null);
        Page<Proposal> proposals = sut.find(walter, "INGREDIENT", "NEW", 0, 5);
        assertEquals(proposals.getTotalPages(), 1);
        assertEquals(proposals.getTotalElements(), 3);

        Proposal proposal = proposals.iterator().next();
        assertEquals(proposal.getProposalType(), "INGREDIENT");
        assertEquals(proposal.getStatus(), "NEW");
        assertEquals(proposal.getQuantity(), 0);
        assertEquals(proposal.getIngredient().size(), 2);
    }

    @Test
    void appointProposal() {
        User security = userService.findByUsername("security").orElse(null);
        Proposal proposal = sut.findById(7L).orElse(null);

        assertNotNull(proposal);
        sut.appointProposal(security, proposal);
        assertEquals(proposal.getStatus(), "ACCEPTED");
    }

    @Test
    void create() {
        User security = userService.findByUsername("security").orElse(null);

        Proposal proposal = Proposal.builder()
                .proposalType("SECURITY")
                .description("Some Desc")
                .place("Some Place")
                .build();

        sut.create(security, proposal);

        assertNotNull(proposal.getId());
        assertNotNull(proposal.getCreatedAt());
        assertEquals(proposal.getStatus(), "NEW");
    }

    @Test
    void changeProposal() {
        Proposal proposal = sut.findById(7L).orElse(null);

        assertNotNull(proposal);
        assertEquals(proposal.getStatus(), "NEW");
        sut.changeProposal("COMPLETED", proposal);

        assertEquals(proposal.getStatus(), "COMPLETED");
    }

    @Test
    void findTasks() {
        User security = userService.findByUsername("security").orElse(null);
        Page<Proposal> proposals = sut.findTasks(security, "ACCEPTED", 0, 5);
        assertEquals(proposals.getTotalPages(), 1);
        assertEquals(proposals.getTotalElements(), 1);

        Proposal proposal = proposals.iterator().next();
        assertEquals(proposal.getProposalType(), "SECURITY");
        assertEquals(proposal.getStatus(), "ACCEPTED");
        assertEquals(proposal.getQuantity(), 0);
        assertNull(proposal.getIngredient());
        assertEquals(proposal.getPlace(), "SPB");
    }
}