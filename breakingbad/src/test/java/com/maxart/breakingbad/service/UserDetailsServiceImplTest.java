package com.maxart.breakingbad.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@Transactional
class UserDetailsServiceImplTest {

    @Autowired
    private UserDetailsServiceImpl sut;

    @Test
    void loadUserByUsername() {
        UserDetails gustavo = sut.loadUserByUsername("gustavo");
        assertEquals(gustavo.getUsername(), "gustavo");
    }
}