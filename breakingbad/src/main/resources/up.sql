-- BREAKING BAD SYSTEM
-- Roles Table
CREATE TABLE IF NOT EXISTS "roles" (
  id   BIGSERIAL   NOT NULL PRIMARY KEY,
  name VARCHAR(16) NOT NULL
);

INSERT INTO roles (name) VALUES
  ('ROLE_CUSTOMER'), ('ROLE_DEALER'), ('ROLE_COOK'),
  ('ROLE_SECURITY'), ('ROLE_PROVIDER'), ('ROLE_CARTEL');

-- Users Table
CREATE TABLE IF NOT EXISTS "users" (
  id       BIGSERIAL                                                     NOT NULL PRIMARY KEY,
  username VARCHAR(64)                                                   NOT NULL,
  password VARCHAR(1024)                                                 NOT NULL,
  role_id  INT REFERENCES roles (id) ON UPDATE CASCADE ON DELETE CASCADE NOT NULL
);

INSERT INTO users (username, password, role_id)
VALUES ('gustavo', '$2a$10$gv9pru4Oxu9yDxfpRu.yiuaE1jr7akQUM2kw55fnnGO3Y74cbUE1C', 6),
  ('provider', '$2a$10$5VR6zfd9CSqkePyUxEll9OmFIk.qe5yj/r46UmSfkAaP0rXr0YlhS', 5),
  ('security', '$2a$10$eZcZUsbGi39HMjyDHeWZQeaeGJUi3ktel/m5tiYixdIXQa.PPaS2a', 4),
  ('walter', '$2a$10$UH0JlSO.YtFfYTqeMS7ieO.FySsSi0eOwzvx.DolYReL8C3Yjj8ku', 3),
  ('dealer', '$2a$10$aWZ5K6Sn36yIYI.hkqzHruh8oLN9EkK9TmzJtyLg6C7ca4p3gfc5m', 2),
  ('dealer1', '$2a$10$7WMWc7PgzcxBuK.l355ukeHAifu2BkZv/MjNnzYRz13FgkxJZgfIa', 2),
  ('customer', '$2a$10$9sAANsCxr5Hml6gRILCboOQqjhES7PxoT.DoGpmq4hQ2u6n1D8PvS', 1),
  ('security1', '$2a$10$uC8/In9I3HaYuhPwZ.C5FOStKujJ23u3i/fffAFbpeOg1Y/t9mTwW', 4),
  ('support', '$2a$10$17B22e3.er846K18BI2BF.fiNspi4KgbcuKcyRikVBMKH5dO3U.Tm', 6);

-- Codes Table
CREATE TABLE IF NOT EXISTS "codes" (
  id         BIGSERIAL                                                     NOT NULL PRIMARY KEY,
  key_code   VARCHAR(16)                                                   NOT NULL,
  expires_at TIMESTAMPTZ                                                   NOT NULL,
  is_active  INT DEFAULT 1                                                 NOT NULL,
  role_id    INT REFERENCES roles (id) ON UPDATE CASCADE ON DELETE CASCADE NOT NULL
);

-- Messages Table
CREATE TABLE IF NOT EXISTS "messages" (
  id           BIGSERIAL                                                     NOT NULL PRIMARY KEY,
  sender_id    INT REFERENCES users (id) ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  recipient_id INT REFERENCES users (id) ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  created_at   TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  text         VARCHAR(4096)                                                 NOT NULL
);

-- Orders Table
CREATE TABLE IF NOT EXISTS "orders" (
  id             BIGSERIAL                                                     NOT NULL PRIMARY KEY,
  customer_id    INT REFERENCES users (id) ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  city           VARCHAR(32)                                                   NOT NULL,
  district       VARCHAR(64)                                                   NOT NULL,
  quantity       INT                                                           NOT NULL,
  amount         DECIMAL(12, 2)                                                NOT NULL,
  status         VARCHAR(16)                                                   NOT NULL,
  created_at     TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  dealer_id      INT REFERENCES users (id) ON UPDATE CASCADE ON DELETE CASCADE,
  delivery_at    TIMESTAMP,
  place          VARCHAR(1024),
  photo          VARCHAR(2048),
  payment_status VARCHAR(16)                                                   NOT NULL
);

INSERT INTO orders (customer_id, city, district, quantity, amount, status, created_at, dealer_id, delivery_at, place, photo, payment_status)
VALUES (7, 'SPB', 'Московский', 15, 300.00, 'WAITING', '2018-06-09 12:44:20.625000', 6, NULL, NULL, NULL, 'UNPAID'),
  (7, 'Saransk', 'Пролетарский', 10, 200.00, 'NEW', '2018-06-09 12:44:52.421000', NULL, NULL, NULL, NULL, 'UNPAID'),
  (7, 'Москва', 'Московская область', 120, 2400.00, 'NEW', '2018-06-09 12:45:12.959000', NULL, NULL, NULL, NULL,
   'UNPAID'),
  (7, 'Санкт-Петербург', 'Невский', 12, 240.00, 'NEW', '2018-06-09 12:45:28.058000', NULL, NULL, NULL, NULL,
   'UNPAID'),
  (7, 'СПБ', 'Павловск', 16, 320.00, 'EXECUTABLE', '2018-06-09 12:45:45.573000', 5, '2018-06-10 12:48:21.717000',
      'Лас-Вентурас', 'https://yandex.ru/maps/-/CBqxqWFDXD', 'UNPAID'),
  (7, 'Огород', 'Район', 12, 240.00, 'NEW', '2018-06-09 12:46:10.717000', NULL, NULL, NULL, NULL, 'UNPAID'),
  (7, 'Moscow', 'Где я?', 160, 3200.00, 'NEW', '2018-06-09 12:47:07.384000', NULL, NULL, NULL, NULL, 'UNPAID'),
  (7, 'Gorod', 'District', 123, 2460.00, 'CANCELED', '2018-06-09 12:47:37.491000', 5, NULL, NULL, NULL, 'UNPAID'),
  (7, 'Владивосток', 'Владивосточный', 1500, 30000.00, 'CANCELED', '2018-06-09 12:47:57.076000', 6, NULL, NULL, NULL,
   'UNPAID'),
  (7, 'Ростов', 'Привет из Ростова', 500, 10000.00, 'COMPLETED', '2018-06-09 12:48:21.717000', 5,
      '2018-06-01 12:48:21.717000', 'Привет', 'https://yandex.ru/maps/-/CBUyIThI2B', 'PAID'),
  (7, 'Магадан', 'Тут холодно', 120, 2400.00, 'COMPLETED', '2018-06-09 12:48:21.717000', 5,
      '2018-06-02 12:48:21.717000', 'Щас согреешься', 'https://yandex.ru/maps/-/CBuAMVhfHD', 'PAID'),
  (7, 'Нью-Йорк', 'Очень много народу', 322, 6440.00, 'COMPLETED', '2018-06-09 12:48:21.717000', 6,
      '2018-06-03 12:48:21.717000', 'Да это жестко', 'https://yandex.ru/maps/-/CBaRqDgwPA', 'PAID'),
  (7, 'Чикаго', 'Пыль, грязь', 20, 400.00, 'COMPLETED', '2018-06-09 12:48:21.717000', 6,
      '2018-06-04 12:48:21.717000', 'В ЛА нормально зато', 'https://yandex.ru/maps/-/CBuAMZF-0C', 'PAID'),
  (7, 'Москва', 'Можно фуру пыли?', 1000, 20000.00, 'COMPLETED', '2018-06-09 12:48:21.717000', 5,
      '2018-06-05 12:48:21.717000', 'Ты на приоре?', 'https://yandex.ru/maps/-/CBqT6YqUSA', 'PAID'),
  (7, 'Майами', 'Мне немножко', 30, 600.00, 'COMPLETED', '2018-06-09 12:48:21.717000', 6,
      '2018-06-06 12:48:21.717000', 'Подходи к пляжу', 'https://yandex.ru/maps/-/CBaEaXu-2A', 'PAID'),
  (7, 'Рязань', 'Рязань - столица мира', 140, 2800.00, 'COMPLETED', '2018-06-09 12:48:21.717000', 6,
      '2018-06-07 12:48:21.717000', 'Ножик в печень, никто не вечен', 'https://yandex.ru/maps/-/CBq-ARTP-B', 'PAID'),
  (7, 'Лос-Анджелес', 'Hello, how a u?', 400, 8000.00, 'COMPLETED', '2018-06-09 12:48:21.717000', 5,
      '2018-06-08 12:48:21.717000', 'Im fine', 'https://yandex.ru/maps/-/CBuAMJEt8D', 'PAID'),
  (7, 'ЛА', 'На пляже тепло, а вы там дальше к экзу готовьтесь', 100, 2000.00, 'COMPLETED', '2018-06-09 12:48:21.717000', 6,
      '2018-06-09 12:48:21.717000', 'Ты сделал мне очень больно', 'https://yandex.ru/maps/-/CBuAMJEt8D', 'PAID'),
  (7, 'Токио', 'Почему в метро такие длинные очереди?', 320, 6400.00, 'COMPLETED', '2018-06-09 12:48:21.717000', 6,
      '2018-06-10 12:48:21.717000', 'Час пик', 'https://yandex.ru/maps/-/CBaIYTV13B', 'PAID'),
  (7, 'Киото', 'Почему в метро такие длинные очереди? x2', 15, 300.00, 'COMPLETED', '2018-06-09 12:48:21.717000', 5,
      '2018-06-11 12:48:21.717000', 'Час пик х2', 'https://yandex.ru/maps/-/CBanRGBJ1B', 'PAID'),
  (7, 'Тверь', 'Я Тверь люблю как маму, люблю свой город детства', 143, 2860.00, 'COMPLETED', '2018-06-09 12:48:21.717000', 5,
      '2018-06-12 12:48:21.717000', 'Миша Круг ты ли это?', 'https://yandex.ru/maps/-/CBUauOXn8D', 'PAID'),
  (7, 'Астана', 'Каеф, мышины летают', 400, 8000.00, 'COMPLETED', '2018-06-09 12:48:21.717000', 6,
      '2018-06-12 12:48:21.717000', 'Астана - будущее', 'https://yandex.ru/maps/-/CBuAM6alxD', 'PAID'),
  (7, 'Уганда', 'Welcome to Uganda', 87, 1740.00, 'COMPLETED', '2018-06-09 12:48:21.717000', 6,
      '2018-06-14 12:48:21.717000', 'DO U NO DO WAE?', 'https://yandex.ru/maps/-/CBuAMJcQTA', 'PAID'),
  (7, 'Караганда', 'Где, где?', 73, 1460.00, 'COMPLETED', '2018-06-09 12:48:21.717000', 5,
      '2018-06-14 12:48:21.717000', 'В караганде, очевидно!', 'https://yandex.ru/maps/-/CBU6f2VmXA', 'PAID');

-- Proposals Table
CREATE TABLE IF NOT EXISTS "proposals" (
  id          BIGSERIAL                                                     NOT NULL PRIMARY KEY,
  type        VARCHAR(16)                                                   NOT NULL,
  user_id     INT REFERENCES users (id) ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  description VARCHAR(4096),
  created_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  place       VARCHAR(1024),
  quantity    INT,
  status      VARCHAR(16)                                                   NOT NULL,
  security_id INT REFERENCES users (id) ON UPDATE CASCADE ON DELETE CASCADE,
  ingredients JSON
);

INSERT INTO proposals (type, user_id, description, created_at, place, quantity, status, security_id, ingredients)
VALUES ('INGREDIENT', 4, NULL, CURRENT_TIMESTAMP, NULL, 0, 'NEW', NULL, '[
  {
    "ingredientName": "Метиламмония ацетат",
    "ingredientPrice": 0,
    "ingredientAmount": 32
  },
  {
    "ingredientName": "Цитата молнии",
    "ingredientPrice": 0,
    "ingredientAmount": 12
  }
]'),
  ('INGREDIENT', 4, NULL, CURRENT_TIMESTAMP, NULL, 0, 'NEW', NULL, '[
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 0,
      "ingredientAmount": 2
    }
  ]'),
  ('CASH', 2, NULL, CURRENT_TIMESTAMP, NULL, 0, 'NEW', NULL, '[
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 32000,
      "ingredientAmount": 0
    },
    {
      "ingredientName": "Цитата молнии",
      "ingredientPrice": 12000,
      "ingredientAmount": 0
    }
  ]'),
  ('CASH', 2, NULL, CURRENT_TIMESTAMP, NULL, 0, 'NEW', NULL, '[
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 2000,
      "ingredientAmount": 0
    }
  ]'),
  ('GOOD', 5, NULL, CURRENT_TIMESTAMP, 'Moscow', 150, 'NEW', NULL, NULL),
  ('GOOD', 6, NULL, CURRENT_TIMESTAMP, 'SPB', 999, 'NEW', NULL, NULL),
  ('SECURITY', 2, 'Срочно помощь!', CURRENT_TIMESTAMP, 'SPB', 0, 'NEW', NULL, NULL),
  ('SECURITY', 3, 'Да где помощь то?', CURRENT_TIMESTAMP, 'Moscow', 0, 'COMPLETED', 8, NULL),
  ('SECURITY', 4, 'HEEEELP!', CURRENT_TIMESTAMP, 'Moscow', 0, 'NEW', NULL, NULL),
  ('SECURITY', 5, 'ХЭЭЭЭЭЛП!', CURRENT_TIMESTAMP, 'SPB', 0, 'ACCEPTED', 3, NULL);

-- Reports Table
CREATE TABLE IF NOT EXISTS "reports" (
  id          BIGSERIAL                                                     NOT NULL PRIMARY KEY,
  created_at  TIMESTAMP DEFAULT NOW(),
  type        VARCHAR(16)                                                   NOT NULL,
  user_id     INT REFERENCES users (id) ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  ingredients JSON                                                          NOT NULL,
  total       DECIMAL(12, 2)                                                NOT NULL
);

INSERT INTO reports (created_at, type, user_id, ingredients, total)
VALUES (CURRENT_TIMESTAMP, 'INGREDIENT', 4, '[
  {
    "ingredientName": "Метиламмония ацетат",
    "ingredientPrice": 0,
    "ingredientAmount": 32
  },
  {
    "ingredientName": "Цитата молнии",
    "ingredientPrice": 0,
    "ingredientAmount": 12
  }
]', 13),
  (CURRENT_TIMESTAMP, 'INGREDIENT', 4, '[
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 0,
      "ingredientAmount": 2
    }
  ]', 2),
  ('2018-06-01 12:48:21.717000', 'CASH', 2, '[
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 32000,
      "ingredientAmount": 0
    },
    {
      "ingredientName": "Цитата молнии",
      "ingredientPrice": 12000,
      "ingredientAmount": 0
    }
  ]', 44000),
  ('2018-06-02 12:48:21.717000', 'CASH', 2, '[
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 2000,
      "ingredientAmount": 0
    }
  ]', 2000),
  ('2018-06-03 12:48:21.717000', 'CASH', 2, '[
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 2000,
      "ingredientAmount": 0
    },
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 2000,
      "ingredientAmount": 0
    },
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 2000,
      "ingredientAmount": 0
    },
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 2000,
      "ingredientAmount": 0
    }
  ]', 8000),
  ('2018-06-04 12:48:21.717000', 'CASH', 2, '[
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 2000,
      "ingredientAmount": 0
    },
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 4000,
      "ingredientAmount": 0
    },
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 2000,
      "ingredientAmount": 0
    },
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 2000,
      "ingredientAmount": 0
    }
  ]', 10000),
  ('2018-06-05 12:48:21.717000', 'CASH', 2, '[
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 2000,
      "ingredientAmount": 0
    },
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 6000,
      "ingredientAmount": 0
    },
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 2000,
      "ingredientAmount": 0
    },
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 2000,
      "ingredientAmount": 0
    }
  ]', 12000),
  ('2018-06-06 12:48:21.717000', 'CASH', 2, '[
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 14000,
      "ingredientAmount": 0
    },
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 2000,
      "ingredientAmount": 0
    },
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 2000,
      "ingredientAmount": 0
    },
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 2000,
      "ingredientAmount": 0
    }
  ]', 22000),
  ('2018-06-07 12:48:21.717000', 'CASH', 2, '[
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 2000,
      "ingredientAmount": 0
    },
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 2000,
      "ingredientAmount": 0
    },
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 2000,
      "ingredientAmount": 0
    },
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 13000,
      "ingredientAmount": 0
    }
  ]', 19000),
  ('2018-06-08 12:48:21.717000', 'CASH', 2, '[
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 2000,
      "ingredientAmount": 0
    }
  ]', 2000),
  ('2018-06-09 12:48:21.717000', 'CASH', 2, '[
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 4000,
      "ingredientAmount": 0
    }
  ]', 4000),
  ('2018-06-10 12:48:21.717000', 'CASH', 2, '[
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 3000,
      "ingredientAmount": 0
    }
  ]', 3000),
  ('2018-06-10 12:48:21.717000', 'CASH', 2, '[
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 5000,
      "ingredientAmount": 0
    }
  ]', 5000),
  ('2018-06-11 12:48:21.717000', 'CASH', 2, '[
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 14000,
      "ingredientAmount": 0
    }
  ]', 14000),
  ('2018-06-12 12:48:21.717000', 'CASH', 2, '[
    {
      "ingredientName": "Метиламмония ацетат",
      "ingredientPrice": 2000,
      "ingredientAmount": 0
    }
  ]', 2000);

-- Function for stats
CREATE OR REPLACE FUNCTION statistics(startAt TIMESTAMP, finishAt TIMESTAMP)
  RETURNS TABLE(date TEXT, incoming NUMERIC, outgoing NUMERIC) AS $$
BEGIN
  RETURN QUERY SELECT
                 coalesce(tbl1.date, tbl2.date) AS dayz,
                 coalesce(total, 0)             AS outgoing,
                 coalesce(amount, 0)            AS incoming
               FROM (SELECT
                       to_char(rep.created_at, 'YYYY-MM-dd') AS date,
                       sum(rep.total)                        AS total
                     FROM reports AS rep
                     WHERE rep.created_at >= startAt AND rep.created_at <= finishAt
                     GROUP BY date
                     ORDER BY date ASC) tbl1
                 NATURAL FULL OUTER JOIN (
                                           SELECT
                                             to_char(ord.delivery_at, 'YYYY-MM-dd') AS date,
                                             sum(ord.amount)                        AS amount
                                           FROM orders AS ord
                                           WHERE ord.status = 'COMPLETED'
                                                 AND ord.delivery_at >= startAt
                                                 AND ord.delivery_at <= finishAt
                                           GROUP BY date
                                           ORDER BY date ASC
                                         ) tbl2;
END;
$$
LANGUAGE plpgsql;