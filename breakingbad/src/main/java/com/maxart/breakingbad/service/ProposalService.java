package com.maxart.breakingbad.service;

import com.maxart.breakingbad.model.Proposal;
import com.maxart.breakingbad.model.User;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface ProposalService {
    Optional<Proposal> findById(Long id);

    Page<Proposal> find(User user, String proposalType, String status, int page, int size);

    void appointProposal(User security, Proposal proposal);

    void create(User user, Proposal proposal);

    void changeProposal(String status, Proposal proposal);

    Page<Proposal> findTasks(User user, String status, int page, int size);
}
