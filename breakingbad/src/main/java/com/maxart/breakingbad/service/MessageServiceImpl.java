package com.maxart.breakingbad.service;

import com.maxart.breakingbad.model.Message;
import com.maxart.breakingbad.model.User;
import com.maxart.breakingbad.repository.MessageRepository;
import com.maxart.breakingbad.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class MessageServiceImpl implements MessageService {

    private static final String incoming = "in";

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public void save(User sender, Message message) {
        message.setSender(sender);
        message.setCreatedAt(new Date());

        User recipient = userRepository.findByUsername(message.getRecipient().getUsername()).orElse(null);
        if (recipient != null) {
            message.setRecipient(recipient);
            messageRepository.save(message);
        }
    }

    @Override
    public Optional<Message> findById(Long id) {
        return messageRepository.findById(id);
    }

    @Override
    public Page<Message> findAll(User user, String part, int page, int size) {
        if (part.equals(incoming)) {
            return messageRepository.findAllByRecipientOrderByCreatedAtDesc(user, PageRequest.of(page, size));
        } else {
            return messageRepository.findAllBySenderOrderByCreatedAtDesc(user, PageRequest.of(page, size));
        }
    }

    @Override
    public String deleteById(User user, Long id) {
        Message message = messageRepository.findById(id).orElse(null);
        if (message != null) {
            if (user.getUsername().equals(message.getSender().getUsername())) {
                messageRepository.deleteById(id);
                return "ok";
            }
        }

        return "error";
    }
}
