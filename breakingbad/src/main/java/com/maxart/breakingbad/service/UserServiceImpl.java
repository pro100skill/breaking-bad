package com.maxart.breakingbad.service;

import com.maxart.breakingbad.model.Code;
import com.maxart.breakingbad.model.Role;
import com.maxart.breakingbad.model.Stats;
import com.maxart.breakingbad.model.User;
import com.maxart.breakingbad.repository.CodeRepository;
import com.maxart.breakingbad.repository.RoleRepository;
import com.maxart.breakingbad.repository.StatsRepository;
import com.maxart.breakingbad.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private StatsRepository statsRepository;

    @Autowired
    private CodeRepository codeRepository;

    @Override
    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

        Role role = roleRepository.findById(1L).orElse(null);
        if (!user.getCode().isEmpty()) {
            Code code = codeRepository
                    .findByKeyCodeAndIsActiveAndExpiresAtGreaterThan(user.getCode(), 1, Calendar.getInstance().getTime())
                    .orElse(null);
            if (code != null) {
                code.setIsActive(0);
                role = code.getRole();
                codeRepository.save(code);
            }
        }

        user.setRole(role);
        userRepository.save(user);
    }

    @Override
    public Optional<User> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public Optional<User> findById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public List<User> findByRole(Long roleId) {
        Role role = roleRepository.findById(roleId).orElse(null);
        if (role != null) {
            return userRepository.findByRole(role);
        }

        return null;
    }

    @Override
    public List<Stats> getStats(Date startAt, Date finishAt) {
        return statsRepository.getStats(startAt, finishAt);
    }
}
