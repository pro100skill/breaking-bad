package com.maxart.breakingbad.service;

import com.maxart.breakingbad.model.Message;
import com.maxart.breakingbad.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface MessageService {
    void save(User sender, Message message);

    Optional<Message> findById(Long id);

    Page<Message> findAll(User user, String part, int page, int size);

    String deleteById(User user, Long id);
}