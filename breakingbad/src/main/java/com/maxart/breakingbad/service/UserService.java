package com.maxart.breakingbad.service;

import com.maxart.breakingbad.model.Message;
import com.maxart.breakingbad.model.Stats;
import com.maxart.breakingbad.model.User;
import org.springframework.data.domain.Page;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface UserService {
    void save(User user);

    Optional<User> findByUsername(String username);

    Optional<User> findById(Long id);

    List<User> findByRole(Long roleId);

    List<Stats> getStats(Date startAt, Date finishAt);
}
