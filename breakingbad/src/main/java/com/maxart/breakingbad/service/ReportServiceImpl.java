package com.maxart.breakingbad.service;

import com.maxart.breakingbad.model.Report;
import com.maxart.breakingbad.model.User;
import com.maxart.breakingbad.repository.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    private ReportRepository reportRepository;

    @Override
    public void create(User user, Report report) {
        report.setUser(user);
        report.setCreatedAt(new Date());
        reportRepository.save(report);
    }
}
