package com.maxart.breakingbad.service;

import com.maxart.breakingbad.model.Code;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface CodeService {
    void save(Code code);

    Optional<Code> findById(Long id);

    Page<Code> findPaginated(int page, int size);
}
