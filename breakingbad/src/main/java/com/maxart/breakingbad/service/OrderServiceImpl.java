package com.maxart.breakingbad.service;

import com.maxart.breakingbad.model.Order;
import com.maxart.breakingbad.model.User;
import com.maxart.breakingbad.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {

    private static final int PRICE = 20;

    @Autowired
    private OrderRepository orderRepository;

    @Override
    public void create(User user, Order order) {
        order.setCustomer(user);
        order.setStatus("NEW");
        order.setAmount(PRICE * order.getQuantity());
        order.setCreatedAt(new Date());
        order.setPaymentStatus("UNPAID");
        orderRepository.save(order);
    }

    @Override
    public void appointDealer(User dealer, Order order) {
        order.setDealer(dealer);
        order.setStatus("WAITING");
        orderRepository.save(order);
    }

    @Override
    public void acceptOrder(Date deliveryAt, String place, String photo, Order order) {
        order.setPlace(place);
        order.setPhoto(photo);
        order.setStatus("EXECUTABLE");
        order.setDeliveryAt(deliveryAt);
        orderRepository.save(order);
    }

    @Override
    public void completeOrder(Order order) {
        order.setStatus("COMPLETED");
        orderRepository.save(order);
    }

    @Override
    public void payOrder(String status, Order order) {
        if (status.equals("CANCELED"))
            order.setStatus("CANCELED");
        else
            order.setPaymentStatus("PAID");

        orderRepository.save(order);
    }

    @Override
    public Optional<Order> findById(Long id) {
        return orderRepository.findById(id);
    }


    @Override
    public Page<Order> find(User user, String status, int page, int size) {
        if (user.getRole().getName().equals("ROLE_CUSTOMER"))
            return orderRepository.findByCustomerAndStatusOrderByCreatedAtDesc(user, status, PageRequest.of(page, size));
        if (user.getRole().getName().equals("ROLE_DEALER"))
            return orderRepository.findByDealerAndStatusOrderByCreatedAtDesc(user, status, PageRequest.of(page, size));

        return orderRepository.findByStatusOrderByCreatedAtDesc(status, PageRequest.of(page, size));
    }
}
