package com.maxart.breakingbad.service;

import com.maxart.breakingbad.model.Proposal;
import com.maxart.breakingbad.model.User;
import com.maxart.breakingbad.repository.ProposalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class ProposalServiceImpl implements ProposalService {

    @Autowired
    private ProposalRepository proposalRepository;

    @Override
    public Optional<Proposal> findById(Long id) {
        return proposalRepository.findById(id);
    }

    @Override
    public Page<Proposal> find(User user, String proposalType, String status, int page, int size) {
        if (user.getRole().getName().equals("ROLE_CARTEL"))
            return proposalRepository.findByProposalTypeAndStatusOrderByCreatedAtDesc(proposalType, status, PageRequest.of(page, size));

        return proposalRepository.findByUserAndStatusOrderByCreatedAtDesc(user, status, PageRequest.of(page, size));
    }

    @Override
    public void appointProposal(User security, Proposal proposal) {
        proposal.setStatus("ACCEPTED");
        proposal.setSecurity(security);
        proposalRepository.save(proposal);
    }

    @Override
    public void create(User user, Proposal proposal) {
        proposal.setStatus("NEW");
        proposal.setUser(user);
        proposal.setCreatedAt(new Date());
        proposalRepository.save(proposal);
    }

    @Override
    public void changeProposal(String status, Proposal proposal) {
        proposal.setStatus(status);
        proposalRepository.save(proposal);
    }

    @Override
    public Page<Proposal> findTasks(User user, String status, int page, int size) {
        return proposalRepository.findBySecurityAndStatusOrderByCreatedAtDesc(user, status, PageRequest.of(page, size));
    }
}
