package com.maxart.breakingbad.service;

import com.maxart.breakingbad.model.Code;
import com.maxart.breakingbad.model.Role;
import com.maxart.breakingbad.repository.CodeRepository;
import com.maxart.breakingbad.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Random;

@Service
public class CodeServiceImpl implements CodeService {

    private static final int MIN_CODE = 100000;
    private static final int MAX_CODE = 999999;

    @Autowired
    private CodeRepository codeRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public Page<Code> findPaginated(int page, int size) {
        return codeRepository.findByIsActiveOrderByExpiresAtDesc(1, PageRequest.of(page, size));
    }

    @Override
    public void save(Code code) {
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt((MAX_CODE - MIN_CODE) + 1) + MIN_CODE;
        code.setKeyCode(String.valueOf(randomInt));
        roleRepository.findById(code.getRole().getId()).ifPresent(code::setRole);
        code.setIsActive(1);
        codeRepository.save(code);
    }

    @Override
    public Optional<Code> findById(Long id) {
        return codeRepository.findById(id);
    }
}
