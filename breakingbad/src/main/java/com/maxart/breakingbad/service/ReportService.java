package com.maxart.breakingbad.service;

import com.maxart.breakingbad.model.Report;
import com.maxart.breakingbad.model.User;

public interface ReportService {
    void create(User user, Report report);
}
