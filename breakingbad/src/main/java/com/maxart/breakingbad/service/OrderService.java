package com.maxart.breakingbad.service;

import com.maxart.breakingbad.model.Order;
import com.maxart.breakingbad.model.User;
import org.springframework.data.domain.Page;

import java.util.Date;
import java.util.Optional;

public interface OrderService {
    void create(User user, Order order);

    void appointDealer(User dealer, Order order);

    void acceptOrder(Date deliveryAt, String place, String photo, Order order);

    void completeOrder(Order order);

    void payOrder(String status, Order order);

    Optional<Order> findById(Long id);

    Page<Order> find(User user, String status, int page, int size);
}
