package com.maxart.breakingbad.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/api/v1/signup").permitAll()
                .antMatchers("/api/v1/orders/").hasAnyRole("CUSTOMER", "DEALER", "CARTEL")
                .antMatchers("/api/v1/proposals/").hasAnyRole("DEALER", "COOK", "SECURITY", "PROVIDER", "CARTEL")
                .antMatchers("/api/v1/proposals/new",
                        "/api/v1/proposals/{id}/change").hasAnyRole("DEALER", "COOK", "SECURITY", "PROVIDER")
                .antMatchers("/api/v1/reports/new").hasAnyRole("COOK", "PROVIDER")
                .antMatchers("/api/v1/orders/new", "/api/v1/orders/{id}/pay").hasRole("CUSTOMER")
                .antMatchers("/api/v1/orders/{id}/accept").hasRole("DEALER")
                .antMatchers("/api/v1/codes/**",
                        "/api/v1/orders/{id}/appoint",
                        "/api/v1/proposals/{id}/appoint",
                        "/api/v1/users",
                        "/api/v1/stats").hasRole("CARTEL")
                .antMatchers("/**").permitAll();
    }
}
