package com.maxart.breakingbad.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@TypeDefs({@TypeDef(name = "IngredientJson", typeClass = IngredientType.class)})
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "reports")
public class Report {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
    private Date createdAt;

    /**
     * Can be INGREDIENT, CASH
     */
    @Column(name = "type")
    @NotNull
    private String reportType;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @NotNull
    private User user;

    @Column(name = "ingredients")
    @Type(type = "IngredientJson")
    private List<Ingredient> ingredient;

    @Column(name = "total")
    private double total;
}
