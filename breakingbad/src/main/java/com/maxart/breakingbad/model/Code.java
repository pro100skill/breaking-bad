package com.maxart.breakingbad.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "codes")
public class Code {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "key_code")
    @NotNull
    private String keyCode;

    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;

    @Column(name = "expires_at")
    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
    private Date expiresAt;

    @Column(name = "is_active")
    private int isActive;
}
