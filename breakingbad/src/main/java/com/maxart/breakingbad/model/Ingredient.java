package com.maxart.breakingbad.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Ingredient implements Serializable {

    private String ingredientName;

    private double ingredientPrice;

    private double ingredientAmount;

}
