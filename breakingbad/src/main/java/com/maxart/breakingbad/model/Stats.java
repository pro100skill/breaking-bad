package com.maxart.breakingbad.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Stats {

    @Id
    @Column(name = "date")
    @NotNull
    private String groupDate;

    @Column(name = "incoming")
    private double incoming;

    @Column(name = "outgoing")
    private double outgoing;

    @Transient
    private Date startAt;

    @Transient
    private Date endAt;
}
