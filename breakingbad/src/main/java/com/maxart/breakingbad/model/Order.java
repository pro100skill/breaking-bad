package com.maxart.breakingbad.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    @NotNull
    private User customer;

    @Column(name = "city")
    @NotNull
    private String city;

    @Column(name = "district")
    @NotNull
    private String district;

    @Column(name = "quantity")
    @NotNull
    private int quantity;

    @Column(name = "amount")
    @NotNull
    private double amount;

    /**
     * Can be NEW, WAITING, EXECUTABLE, COMPLETED, CANCELED
     */
    @Column(name = "status")
    @NotNull
    private String status;

    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @ManyToOne
    @JoinColumn(name = "dealer_id")
    private User dealer;

    @Column(name = "delivery_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deliveryAt;

    @Column(name = "place")
    private String place;

    @Column(name = "photo")
    private String photo;

    /**
     * Can be PAID, UNPAID
     */
    @Column(name = "payment_status")
    @NotNull
    private String paymentStatus;
}
