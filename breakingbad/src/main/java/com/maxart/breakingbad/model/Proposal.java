package com.maxart.breakingbad.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@TypeDefs({@TypeDef(name = "IngredientJson", typeClass = IngredientType.class)})
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "proposals")
public class Proposal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Can be INGREDIENT, SECURITY, CASH, GOOD
     */
    @Column(name = "type")
    @NotNull
    private String proposalType;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @NotNull
    private User user;

    @Column(name = "description")
    private String description;

    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
    private Date createdAt;

    @Column(name = "place")
    private String place;

    @Column(name = "quantity")
    private int quantity;

    /**
     * Can be NEW, ACCEPTED, CANCELED, COMPLETED
     */
    @Column(name = "status")
    @NotNull
    private String status;

    @ManyToOne
    @JoinColumn(name = "security_id")
    private User security;

    @Column(name = "ingredients")
    @Type(type = "IngredientJson")
    private List<Ingredient> ingredient;
}

