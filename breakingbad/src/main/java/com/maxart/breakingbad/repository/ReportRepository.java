package com.maxart.breakingbad.repository;

import com.maxart.breakingbad.model.Report;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportRepository extends JpaRepository<Report, Long> {
}
