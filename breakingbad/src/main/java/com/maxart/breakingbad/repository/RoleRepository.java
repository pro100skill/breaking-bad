package com.maxart.breakingbad.repository;

import com.maxart.breakingbad.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
