package com.maxart.breakingbad.repository;

import com.maxart.breakingbad.model.Stats;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface StatsRepository extends JpaRepository<Stats, Long> {

    @Query(value = "SELECT * FROM statistics(?1, ?2)", nativeQuery = true)
    List<Stats> getStats(Date startAt, Date finishAt);
}
