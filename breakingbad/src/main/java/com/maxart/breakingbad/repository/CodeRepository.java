package com.maxart.breakingbad.repository;

import com.maxart.breakingbad.model.Code;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.Optional;

public interface CodeRepository extends JpaRepository<Code, Long> {

    Optional<Code> findByKeyCodeAndIsActiveAndExpiresAtGreaterThan(String keyCode, int isActive, Date expiresAt);

    Page<Code> findByIsActiveOrderByExpiresAtDesc(int isActive, Pageable pageable);

}
