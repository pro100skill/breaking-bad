package com.maxart.breakingbad.repository;

import com.maxart.breakingbad.model.Proposal;
import com.maxart.breakingbad.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProposalRepository extends JpaRepository<Proposal, Long> {

    Page<Proposal> findBySecurityAndStatusOrderByCreatedAtDesc(User user, String status, Pageable pageable);

    Page<Proposal> findByUserAndStatusOrderByCreatedAtDesc(User user, String status, Pageable pageable);

    Page<Proposal> findByProposalTypeAndStatusOrderByCreatedAtDesc(String proposalType, String status, Pageable pageable);
}
