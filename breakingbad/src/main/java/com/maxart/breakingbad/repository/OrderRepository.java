package com.maxart.breakingbad.repository;

import com.maxart.breakingbad.model.Order;
import com.maxart.breakingbad.model.Stats;
import com.maxart.breakingbad.model.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long> {
    Page<Order> findByCustomerAndStatusOrderByCreatedAtDesc(User user, String status, Pageable pageable);

    Page<Order> findByDealerAndStatusOrderByCreatedAtDesc(User user, String status, Pageable pageable);

    Page<Order> findByStatusOrderByCreatedAtDesc(String status, Pageable pageable);
}