package com.maxart.breakingbad.repository;

import com.maxart.breakingbad.model.Message;
import com.maxart.breakingbad.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepository extends JpaRepository<Message, Long> {
    Page<Message> findAllByRecipientOrderByCreatedAtDesc(User user, Pageable pageable);

    Page<Message> findAllBySenderOrderByCreatedAtDesc(User user, Pageable pageable);
}
