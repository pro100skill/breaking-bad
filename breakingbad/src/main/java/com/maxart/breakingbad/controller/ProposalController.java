package com.maxart.breakingbad.controller;

import com.maxart.breakingbad.exceptions.ResourceNotFoundException;
import com.maxart.breakingbad.model.Proposal;
import com.maxart.breakingbad.model.User;
import com.maxart.breakingbad.service.ProposalService;
import com.maxart.breakingbad.service.UserService;
import com.maxart.breakingbad.validator.ProposalValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/proposals")
public class ProposalController {

    @Autowired
    private UserService userService;

    @Autowired
    private ProposalService proposalService;

    @Autowired
    private ProposalValidator proposalValidator;

    @PostMapping(value = "/new")
    public ResponseEntity<?> createProposal(@RequestBody Proposal proposal, BindingResult bindingResult, Principal principal) {
        proposalValidator.validate(proposal, bindingResult);
        if (bindingResult.hasErrors()) {
            List<FieldError> fieldErrors = bindingResult.getFieldErrors();
            return ResponseEntity.ok(fieldErrors);
        }

        User user = userService.findByUsername(principal.getName()).orElse(null);
        if (user == null) {
            throw new ResourceNotFoundException();
        }

        proposalService.create(user, proposal);
        return ResponseEntity.ok(proposal);
    }

    @GetMapping(value = "/{id}/appoint", params = {"security"})
    public ResponseEntity<?> appointProposal(@PathVariable("id") Long id,
                                             @RequestParam(value = "security", required = false) String security) {
        Proposal proposal = proposalService.findById(id).orElse(null);
        if (proposal == null) {
            throw new ResourceNotFoundException();
        }

        User sec = userService.findByUsername(security).orElse(null);
        if (sec == null && proposal.getProposalType().equals("SECURITY")) {
            throw new ResourceNotFoundException();
        }

        proposalService.appointProposal(sec, proposal);
        return ResponseEntity.ok(proposal);
    }

    @GetMapping(value = "/{id}/change", params = {"status"})
    public ResponseEntity<?> changeProposal(@PathVariable("id") Long id,
                                            @RequestParam(value = "status") String status,
                                            Principal principal) {
        Proposal proposal = proposalService.findById(id).orElse(null);
        if (proposal == null) {
            throw new ResourceNotFoundException();
        }

        User user = userService.findByUsername(principal.getName()).orElse(null);
        if (user == null || !user.getUsername().equals(proposal.getUser().getUsername())) {
            throw new ResourceNotFoundException();
        }

        proposalService.changeProposal(status, proposal);
        return ResponseEntity.ok(proposal);
    }

    @GetMapping(value = "/", params = {"page", "size", "type", "status"})
    public ResponseEntity<?> index(@RequestParam("page") int page, @RequestParam("size") int size,
                                   @RequestParam("type") String proposalType, @RequestParam("status") String status,
                                   Principal principal) {
        User user = userService.findByUsername(principal.getName()).orElse(null);
        if (user == null) {
            throw new ResourceNotFoundException();
        }

        Page<Proposal> proposals = proposalService.find(user, proposalType, status, page, size);
        return ResponseEntity.ok(proposals);
    }

    @GetMapping(value = "/tasks/", params = {"page", "size", "status"})
    public ResponseEntity<?> getTasks(@RequestParam("page") int page, @RequestParam("size") int size,
                                      @RequestParam("status") String status,
                                      Principal principal) {
        User user = userService.findByUsername(principal.getName()).orElse(null);
        if (user == null || !user.getRole().getName().equals("ROLE_SECURITY")) {
            throw new ResourceNotFoundException();
        }

        Page<Proposal> proposals = proposalService.findTasks(user, status, page, size);
        return ResponseEntity.ok(proposals);
    }
}