package com.maxart.breakingbad.controller;

import com.maxart.breakingbad.exceptions.ResourceNotFoundException;
import com.maxart.breakingbad.model.Message;
import com.maxart.breakingbad.model.User;
import com.maxart.breakingbad.service.MessageService;
import com.maxart.breakingbad.service.UserService;
import com.maxart.breakingbad.validator.MessageValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/messages")
public class MessageController {

    private static final String incoming = "in";
    private static final String outgoing = "out";

    @Autowired
    private MessageValidator messageValidator;

    @Autowired
    private MessageService messageService;

    @Autowired
    private UserService userService;

    @GetMapping(value = "/", params = {"page", "size", "part"})
    public ResponseEntity<?> index(@RequestParam("page") int page, @RequestParam("size") int size, @RequestParam("part") String part, Principal principal) {
        User user = userService.findByUsername(principal.getName()).orElse(null);
        if (user == null || !(part.equals(outgoing) || part.equals(incoming))) {
            throw new ResourceNotFoundException();
        }

        Page<Message> messages = messageService.findAll(user, part, page, size);

        return ResponseEntity.ok(messages);
    }

    @PostMapping(value = "/new")
    public ResponseEntity<?> create(@RequestBody Message message, BindingResult bindingResult, Principal principal) {
        User user = userService.findByUsername(principal.getName()).orElse(null);
        if (user == null) {
            throw new ResourceNotFoundException();
        }

        messageValidator.validate(message, bindingResult);

        if (bindingResult.hasErrors()) {
            List<FieldError> fieldErrors = bindingResult.getFieldErrors();
            return ResponseEntity.ok(fieldErrors);
        }

        messageService.save(user, message);
        return ResponseEntity.ok(message);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> index(@PathVariable("id") Long id, Principal principal) {
        User user = userService.findByUsername(principal.getName()).orElse(null);
        Message message = messageService.findById(id).orElse(null);

        if (message == null || user == null) {
            throw new ResourceNotFoundException();
        }

        if (message.getSender().getUsername().equals(user.getUsername()) ||
                message.getRecipient().getUsername().equals(user.getUsername())) {
            return ResponseEntity.ok(message);
        } else {
            throw new ResourceNotFoundException();
        }
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id, Principal principal) {
        User user = userService.findByUsername(principal.getName()).orElse(null);
        if (user == null) {
            throw new ResourceNotFoundException();
        }

        String status = messageService.deleteById(user, id);
        if (status.equals("ok"))
            return ResponseEntity.status(200).body(null);
        else
            throw new ResourceNotFoundException();
    }
}