package com.maxart.breakingbad.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/")
public class MainController {
    @RequestMapping(value = "{path:[^\\.]*}")
    public String redirect() {
        return "forward:/";
    }
}
