package com.maxart.breakingbad.controller;

import com.maxart.breakingbad.exceptions.ResourceNotFoundException;
import com.maxart.breakingbad.model.Stats;
import com.maxart.breakingbad.model.User;
import com.maxart.breakingbad.service.UserService;
import com.maxart.breakingbad.validator.StatsValidator;
import com.maxart.breakingbad.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping(value = "/api/v1")
public class AuthController {

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private StatsValidator statsValidator;

    @Autowired
    private UserService userService;

    @PostMapping(value = "/signup")
    public ResponseEntity<?> register(@RequestBody User user, BindingResult bindingResult) {
        userValidator.validate(user, bindingResult);

        if (bindingResult.hasErrors()) {
            List<FieldError> fieldErrors = bindingResult.getFieldErrors();
            return ResponseEntity.ok(fieldErrors);
        }

        userService.save(user);
        return ResponseEntity.ok(user);
    }

    @GetMapping(value = "/users", params = {"role"})
    public ResponseEntity<?> getUsers(@RequestParam("role") Long roleId) {
        List<User> users = userService.findByRole(roleId);
        if (users == null)
            throw new ResourceNotFoundException();

        return ResponseEntity.ok(users);
    }

    @PostMapping(value = "/stats")
    public ResponseEntity<?> getStats(@RequestBody Stats stats, BindingResult bindingResult) throws ParseException {
        statsValidator.validate(stats, bindingResult);

        if (bindingResult.hasErrors()) {
            List<FieldError> fieldErrors = bindingResult.getFieldErrors();
            return ResponseEntity.ok(fieldErrors);
        }

        List<Stats> statistics = userService.getStats(stats.getStartAt(), stats.getEndAt());
        return ResponseEntity.ok(statistics);
    }
}
