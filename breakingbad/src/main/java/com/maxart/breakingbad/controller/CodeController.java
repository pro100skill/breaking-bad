package com.maxart.breakingbad.controller;

import com.maxart.breakingbad.exceptions.ResourceNotFoundException;
import com.maxart.breakingbad.model.Code;
import com.maxart.breakingbad.service.CodeService;
import com.maxart.breakingbad.validator.CodeValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/codes")
public class CodeController {

    @Autowired
    private CodeValidator codeValidator;

    @Autowired
    private CodeService codeService;

    @GetMapping(value = "/", params = {"page", "size"})
    public ResponseEntity<?> index(@RequestParam("page") int page, @RequestParam("size") int size) {
        Page<Code> codePage = codeService.findPaginated(page, size);
        if (page > codePage.getTotalPages()) {
            throw new ResourceNotFoundException();
        }

        return ResponseEntity.ok(codePage);
    }

    @PostMapping(value = "/new")
    public ResponseEntity<?> create(@RequestBody Code code, BindingResult bindingResult) {
        codeValidator.validate(code, bindingResult);

        if (bindingResult.hasErrors()) {
            List<FieldError> fieldErrors = bindingResult.getFieldErrors();
            return ResponseEntity.ok(fieldErrors);
        }

        codeService.save(code);
        return ResponseEntity.ok(code);
    }
}
