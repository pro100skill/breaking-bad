package com.maxart.breakingbad.controller;

import com.maxart.breakingbad.exceptions.ResourceNotFoundException;
import com.maxart.breakingbad.model.Order;
import com.maxart.breakingbad.model.Stats;
import com.maxart.breakingbad.model.User;
import com.maxart.breakingbad.service.OrderService;
import com.maxart.breakingbad.service.UserService;
import com.maxart.breakingbad.validator.OrderValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/orders")
public class OrderController {

    @Autowired
    private UserService userService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderValidator orderValidator;

    @PostMapping(value = "/new")
    public ResponseEntity<?> createOrder(@RequestBody Order order, BindingResult bindingResult, Principal principal) {
        User user = userService.findByUsername(principal.getName()).orElse(null);
        if (user == null) {
            throw new ResourceNotFoundException();
        }

        orderValidator.validate(order, bindingResult);

        if (bindingResult.hasErrors()) {
            List<FieldError> fieldErrors = bindingResult.getFieldErrors();
            return ResponseEntity.ok(fieldErrors);
        }

        orderService.create(user, order);
        return ResponseEntity.ok(order);
    }

    @PostMapping(value = "/{id}/appoint")
    public ResponseEntity<?> appointOrder(@PathVariable("id") Long id, @RequestBody Order order) {
        Order oldOrder = orderService.findById(id).orElse(null);
        if (oldOrder == null) {
            throw new ResourceNotFoundException();
        }

        User dealer = userService.findByUsername(order.getDealer().getUsername()).orElse(null);
        if (dealer == null) {
            throw new ResourceNotFoundException();
        }

        orderService.appointDealer(dealer, oldOrder);
        return ResponseEntity.ok(oldOrder);
    }

    @PostMapping(value = "/{id}/accept")
    public ResponseEntity<?> acceptOrder(@PathVariable("id") Long id, @RequestBody Order order, BindingResult bindingResult, Principal principal) {
        orderValidator.validate(order, bindingResult);
        if (bindingResult.hasErrors()) {
            List<FieldError> fieldErrors = bindingResult.getFieldErrors();
            return ResponseEntity.ok(fieldErrors);
        }

        Order oldOrder = orderService.findById(id).orElse(null);
        if (oldOrder == null) {
            throw new ResourceNotFoundException();
        }

        User dealer = userService.findByUsername(principal.getName()).orElse(null);
        if (dealer == null || !dealer.getUsername().equals(oldOrder.getDealer().getUsername())) {
            throw new ResourceNotFoundException();
        }

        orderService.acceptOrder(order.getDeliveryAt(), order.getPlace(), order.getPhoto(), oldOrder);
        return ResponseEntity.ok(oldOrder);
    }

    @GetMapping(value = "/{id}/complete")
    public ResponseEntity<?> completeOrder(@PathVariable("id") Long id, Principal principal) {
        Order order = orderService.findById(id).orElse(null);
        if (order == null) {
            throw new ResourceNotFoundException();
        }

        User dealer = userService.findByUsername(principal.getName()).orElse(null);
        if (dealer == null ||
                !dealer.getUsername().equals(order.getDealer().getUsername()) ||
                !order.getPaymentStatus().equals("PAID")) {
            throw new ResourceNotFoundException();
        }

        orderService.completeOrder(order);
        return ResponseEntity.ok(order);
    }

    @GetMapping(value = "/{id}/pay")
    public ResponseEntity<?> payOrder(@PathVariable("id") Long id, @RequestParam("status") String status, Principal principal) {
        Order order = orderService.findById(id).orElse(null);
        if (order == null) {
            throw new ResourceNotFoundException();
        }

        User user = userService.findByUsername(principal.getName()).orElse(null);
        if (user == null || !user.getUsername().equals(order.getCustomer().getUsername())) {
            throw new ResourceNotFoundException();
        }

        orderService.payOrder(status, order);
        return ResponseEntity.ok(order);
    }

    @GetMapping(value = "/", params = {"page", "size", "status"})
    public ResponseEntity<?> index(@RequestParam("page") int page, @RequestParam("size") int size, @RequestParam("status") String status, Principal principal) {
        User user = userService.findByUsername(principal.getName()).orElse(null);
        if (user == null) {
            throw new ResourceNotFoundException();
        }

        Page<Order> orders = orderService.find(user, status, page, size);
        return ResponseEntity.ok(orders);
    }
}