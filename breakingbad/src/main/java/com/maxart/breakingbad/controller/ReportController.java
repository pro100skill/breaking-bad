package com.maxart.breakingbad.controller;

import com.maxart.breakingbad.exceptions.ResourceNotFoundException;
import com.maxart.breakingbad.model.Report;
import com.maxart.breakingbad.model.User;
import com.maxart.breakingbad.service.ReportService;
import com.maxart.breakingbad.service.UserService;
import com.maxart.breakingbad.validator.ReportValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/reports")
public class ReportController {

    @Autowired
    private UserService userService;

    @Autowired
    private ReportService reportService;

    @Autowired
    private ReportValidator reportValidator;

    @PostMapping(value = "/new")
    public ResponseEntity<?> createReport(@RequestBody Report report, BindingResult bindingResult, Principal principal) {
        User user = userService.findByUsername(principal.getName()).orElse(null);
        if (user == null) {
            throw new ResourceNotFoundException();
        }

        reportValidator.validate(report, bindingResult);
        if (bindingResult.hasErrors()) {
            List<FieldError> fieldErrors = bindingResult.getFieldErrors();
            return ResponseEntity.ok(fieldErrors);
        }

        reportService.create(user, report);
        return ResponseEntity.ok(report);
    }
}