package com.maxart.breakingbad.validator;

import com.maxart.breakingbad.model.Stats;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class StatsValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Stats.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Stats stats = (Stats) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "startAt", "Required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "endAt", "Required");
        if (stats.getStartAt().compareTo(stats.getEndAt()) >= 0) {
            errors.rejectValue("endAt", "ExpiredDate.stats.endAt");
        }
    }
}
