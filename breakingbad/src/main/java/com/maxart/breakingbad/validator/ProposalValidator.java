package com.maxart.breakingbad.validator;

import com.maxart.breakingbad.model.Ingredient;
import com.maxart.breakingbad.model.Proposal;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.Date;

@Component
public class ProposalValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Proposal.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Proposal proposal = (Proposal) o;

        if (proposal.getProposalType().equals("GOOD")) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "quantity", "required");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "place", "required");
            if (proposal.getQuantity() <= 0) {
                errors.rejectValue("quantity", "Negative.proposal.quantity");
            }
        }

        if (proposal.getProposalType().equals("SECURITY")) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description", "required");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "place", "required");
        }

        if (proposal.getProposalType().equals("INGREDIENT") || proposal.getProposalType().equals("CASH")) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "ingredient", "required");
            if (proposal.getIngredient().size() == 0) {
                errors.rejectValue("ingredient", "NotExist.proposal.ingredient");
            }  else {
                for (Ingredient ingr : proposal.getIngredient()) {
                    if (ingr.getIngredientPrice() < 0)
                        errors.rejectValue("ingredient", "Negative.proposal.ingredient.ingredientPrice");

                    if (ingr.getIngredientAmount() < 0) {
                        errors.rejectValue("ingredient", "Negative.proposal.ingredient.ingredientAmount");
                    }
                }
            }
        }
    }
}

