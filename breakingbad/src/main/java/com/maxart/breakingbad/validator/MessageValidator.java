package com.maxart.breakingbad.validator;

import com.maxart.breakingbad.model.Message;
import com.maxart.breakingbad.model.User;
import com.maxart.breakingbad.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class MessageValidator implements Validator {

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> clazz) {
        return Message.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Message message = (Message) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "recipient.username", "required");
        User user = userService.findByUsername(message.getRecipient().getUsername()).orElse(null);
        if (user == null) {
            errors.rejectValue("recipient.username", "NotExist.message.recipient.username");
        }
    }
}

