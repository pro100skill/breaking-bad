package com.maxart.breakingbad.validator;

import com.maxart.breakingbad.model.Code;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.Date;


@Component
public class CodeValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return Code.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Code code = (Code) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "expiresAt", "required");
        if (code.getExpiresAt().compareTo(new Date()) < 0) {
            errors.rejectValue("expiresAt", "ExpiredDate.code.expiresAt");
        }
    }
}
