package com.maxart.breakingbad.validator;

import com.maxart.breakingbad.model.Ingredient;
import com.maxart.breakingbad.model.Report;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class ReportValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Report.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Report report = (Report) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "total", "required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "ingredient", "required");
        if (report.getTotal() <= 0) {
            errors.rejectValue("total", "Negative.report.total");
        }

        if (report.getIngredient().size() == 0) {
            errors.rejectValue("ingredient", "NotExist.report.ingredient");
        } else {
            for (Ingredient ingr : report.getIngredient()) {
                if (ingr.getIngredientPrice() < 0)
                    errors.rejectValue("ingredient", "Negative.report.ingredient.ingredientPrice");

                if (ingr.getIngredientAmount() < 0) {
                    errors.rejectValue("ingredient", "Negative.report.ingredient.ingredientAmount");
                }
            }
        }
    }
}


