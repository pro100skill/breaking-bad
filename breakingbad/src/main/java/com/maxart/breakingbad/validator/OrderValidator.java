package com.maxart.breakingbad.validator;

import com.maxart.breakingbad.model.Order;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.Date;

@Component
public class OrderValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Order.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Order order = (Order) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "quantity", "required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "city", "required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "district", "required");
        if (order.getQuantity() <= 0) {
            errors.rejectValue("quantity", "Negative.order.quantity");
        }

        if (order.getDeliveryAt() != null) {
            if (order.getDeliveryAt().compareTo(new Date()) < 0) {
                errors.rejectValue("deliveryAt", "ExpiredDate.order.deliveryAt");
            }
        }
    }
}

