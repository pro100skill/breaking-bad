/* eslint-disable no-param-reassign */
import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import qs from 'qs';

import { proxyLink } from './utils';

Vue.use(Vuex);
const AUTH_REQUEST = 'AUTH_REQUEST';
const AUTH_SUCCESS = 'AUTH_SUCCESS';
const AUTH_ERROR = 'AUTH_ERROR';
const AUTH_LOGOUT = 'AUTH_LOGOUT';
const options = {
  state: {
    token: window.localStorage.getItem('token') || '',
    role: window.localStorage.getItem('role'),
    status: '',
  },
  mutations: {
    [AUTH_REQUEST]: (state) => {
      state.status = 'loading';
    },
    [AUTH_SUCCESS]: (state, resp) => {
      state.status = 'success';
      state.token = resp.token;
      state.role = resp.role;
    },
    [AUTH_ERROR]: (state) => {
      state.status = 'error';
    },
    [AUTH_LOGOUT]: (state) => {
      state.token = '';
      state.role = '';
    },
  },
  actions: {
    [AUTH_REQUEST]: ({ commit }, user) => new Promise((resolve, reject) => {
      commit(AUTH_REQUEST);
      axios.post(
        proxyLink('/oauth/token'),
        qs.stringify({
          grant_type: 'password',
          username: user.username,
          password: user.password,
        }),
        {
          auth: {
            username: 'vuejs-client',
            password: 'ze3sY0lsFoVn',
          },
        },
      ).then((resp) => {
        window.localStorage.setItem('token', resp.data.access_token);
        window.localStorage.setItem('role', resp.data.authorities[0].authority);
        axios.defaults.headers.common.Authorization = resp.data.access_token;
        commit(AUTH_SUCCESS, resp);
        resolve(resp);
      }).catch((err) => {
        commit(AUTH_ERROR, err);
        window.localStorage.removeItem('token');
        window.localStorage.removeItem('role');
        reject(err);
      });
    }),
    [AUTH_LOGOUT]: ({ commit }) => new Promise((resolve) => {
      commit(AUTH_LOGOUT);
      window.localStorage.removeItem('token');
      window.localStorage.removeItem('role');
      window.location.replace('/');
      resolve();
    }),
  },
  getters: {
    isAuthenticated: state => !!state.token,
    userRole: state => state.role,
    authStatus: state => state.status,
  },
};

export default new Vuex.Store(options);
export { options };
