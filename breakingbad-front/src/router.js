import Vue from 'vue';
import Router from 'vue-router';
import store from './store';

import Index from './views/Index.vue';
import Messages from './views/Messages.vue';
import Reports from './views/Reports.vue';
import Proposals from './views/Proposals.vue';
import Tasks from './views/Tasks.vue';
import Orders from './views/Orders.vue';
import Codes from './views/Codes.vue';
import Statistic from './views/Statistic.vue';
import NotFound from './views/NotFound.vue';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  linkActiveClass: 'link__active',
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index,
      meta: {
        requiredAuth: false,
        requiredRoles: ['*'],
      },
    },
    {
      path: '/reports',
      name: 'Reports',
      component: Reports,
      meta: {
        requiredAuth: true,
        requiredRoles: ['ROLE_COOK', 'ROLE_PROVIDER'],
      },
    },
    {
      path: '/tasks',
      name: 'Tasks',
      component: Tasks,
      meta: {
        requiredAuth: true,
        requiredRoles: ['ROLE_SECURITY'],
      },
    },
    {
      path: '/proposals',
      name: 'Proposals',
      component: Proposals,
      meta: {
        requiredAuth: true,
        requiredRoles: ['ROLE_CARTEL', 'ROLE_SECURITY', 'ROLE_COOK', 'ROLE_PROVIDER', 'ROLE_DEALER'],
      },
    },
    {
      path: '/orders',
      name: 'Orders',
      component: Orders,
      meta: {
        requiredAuth: true,
        requiredRoles: ['ROLE_CUSTOMER', 'ROLE_DEALER', 'ROLE_CARTEL'],
      },
    },
    {
      path: '/messages',
      name: 'Messages',
      component: Messages,
      meta: {
        requiredAuth: true,
        requiredRoles: ['*'],
      },
    },
    {
      path: '/statistic',
      name: 'Statistic',
      component: Statistic,
      meta: {
        requiredAuth: true,
        requiredRoles: ['ROLE_CARTEL'],
      },
    },
    {
      path: '/codes',
      name: 'Codes',
      component: Codes,
      meta: {
        requiredAuth: true,
        requiredRoles: ['ROLE_CARTEL'],
      },
    },
    {
      path: '/404',
      name: '404',
      component: NotFound,
    }, {
      path: '*',
      redirect: '/404',
    },
  ],
});

router.beforeEach((to, from, next) => {
  const currentAuth = store.getters.isAuthenticated;
  const currentRole = store.getters.userRole;
  const authReq = to.meta.requiredAuth;
  const rolesReq = to.meta.requiredRoles;
  if (authReq) {
    if (currentAuth) {
      if (rolesReq.includes('*') || rolesReq.includes(currentRole)) {
        next();
      } else {
        next({
          path: '/messages',
        });
      }
    } else {
      next({
        path: '/',
      });
    }
  } else if (currentAuth) {
    next({
      path: '/messages',
    });
  } else {
    next();
  }
});

export default router;
