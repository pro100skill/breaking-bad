/* eslint-disable import/prefer-default-export,no-param-reassign */
export function formDataToObject(formData) {
  const object = {};
  formData.forEach((value, key) => {
    object[key] = value;
  });

  return object;
}

export function codeToMessage(code) {
  const msg = {
    'ExpiredDate.code.expiresAt': 'Неверная дата',
    'ExpiredDate.order.deliveryAt': 'Неверная дата',
    'ExpiredDate.stats.endAt': 'Неверный диапазон дат',
    'Size.user.username': 'Длина имени должна быть от 6 до 32 символов',
    'Duplicate.user.username': 'Имя пользователя занято',
    'Size.user.password': 'Длина пароля должна быть от 6 до 32 символов',
    'Different.user.password': 'Пароли не совпадают',
    'NotExist.message.recipient.username': 'Пользователь не найден',
    'Negative.order.quantity': 'Количество товара не может быть меньше нуля',
    'Negative.proposal.quantity': 'Количество товара не может быть меньше нуля',
    'NotExist.proposal.ingredient': 'Ингредиенты не указаны',
    'Negative.proposal.ingredient.ingredientPrice': 'Цена ингредиента не может быть меньше нуля',
    'Negative.proposal.ingredient.ingredientAmount': 'Объем ингредиента не может быть меньше нуля',
    'Negative.report.total': 'Итого не может быть меньше нуля',
    'NotExist.report.ingredient': 'Ингредиенты не указаны',
    'Negative.report.ingredient.ingredientPrice': 'Цена ингредиента не может быть меньше нуля',
    'Negative.report.ingredient.ingredientAmount': 'Объем ингредиента не может быть меньше нуля',
  };

  return msg[code];
}

export function proxyLink(link) {
  const baseUrl = 'http://localhost:8080';
  if (process.env.VUE_APP_DEBUG) {
    link = baseUrl + link;
  }

  return link;
}
