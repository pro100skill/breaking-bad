import moxios from 'moxios';
import Axios from 'axios';

import { shallowMount, createLocalVue } from '@vue/test-utils';
import { expect } from 'chai';
import MessageForm from '@/components/messages/MessageForm.vue';
import MessageShow from '@/components/messages/MessageShow.vue';

import store from '../../src/store';
import { proxyLink } from '../../src/utils';

const localVue = createLocalVue();
localVue.use(Axios);
localVue.prototype.$http = Axios;

describe('MessageForm.vue', () => {
  beforeEach(() => {
    moxios.install();
  });

  afterEach(() => {
    moxios.uninstall();
  });

  const wrapper = shallowMount(MessageForm, {
    mocks: {
      $store: store,
    },
    localVue,
    propsData: {
      username: 'testName',
    },
  });

  it('renders input props', () => {
    expect(wrapper.props().username).to.equal('testName');
    expect(wrapper.vm.message.recipient.username).to.equal('testName');
  });

  it('renders the correct markup', () => {
    expect(wrapper.html()).to.include('<form action="/api/v1/messages/new" class="form">');
  });

  it('has a button', () => {
    expect(wrapper.contains('.submit')).to.be.true;
  });

  it('send data', (done) => {
    const msg = {
      recipient: {
        username: 'test',
      },
      text: 'hello',
    };

    wrapper.setData({ message: msg });
    wrapper.vm.sendMessage();
    moxios.stubRequest(proxyLink('/api/v1/messages/new'), {
      status: 200,
      response: [{
        code: 'NotExist.message.recipient.username',
      }],
    });

    moxios.wait(() => {
      expect(wrapper.vm.err).to.equal('Пользователь не найден');
      done();
    });
  });
});

describe('MessageShow.vue', () => {
  const wrapper = shallowMount(MessageShow, {
    mocks: {
      $store: store,
    },
    localVue,
    propsData: {
      id: 13,
      username: 'testName',
      text: 'Hello',
      date: (new Date()).toISOString(),
      isSender: true,
    },
  });

  it('renders input props', () => {
    expect(wrapper.props().id).to.equal(13);
    expect(wrapper.props().username).to.equal('testName');
  });

  it('renders the correct markup for sender', () => {
    expect(wrapper.html()).to.include('<div class="submit submit__delete">Удалить</div>');
  });
});
