import moxios from 'moxios';
import Axios from 'axios';

import { shallowMount, createLocalVue } from '@vue/test-utils';
import { expect } from 'chai';
import ProposalsNew from '@/components/proposals/ProposalsNew.vue';
import ProposalsShow from '@/components/proposals/ProposalsShow.vue';

import store from '../../src/store';
import { proxyLink } from '../../src/utils';

const localVue = createLocalVue();
localVue.use(Axios);
localVue.prototype.$http = Axios;

describe('ProposalsNew.vue', () => {
  beforeEach(() => {
    moxios.install();
  });

  afterEach(() => {
    moxios.uninstall();
  });

  const wrapper = shallowMount(ProposalsNew, {
    mocks: {
      $store: store,
    },
    localVue,
  });

  const prop = {
    proposalType: 'GOOD',
    place: 'Place',
    quantity: 20,
  };

  wrapper.setData({ proposal: prop });

  it('renders the correct markup', () => {
    expect(wrapper.html()).to.include('<form action="/api/v1/proposals/new" class="form">');
    expect(wrapper.html()).to.include('<h1 class="h1">Создать заявку</h1>');
  });

  it('has a button', () => {
    expect(wrapper.contains('.submit')).to.be.true;
  });

  it('set and check data', () => {
    expect(wrapper.vm.proposal.place).to.equal('Place');
    expect(wrapper.vm.proposal.quantity).to.equal(20);
    expect(wrapper.vm.proposal.total).to.equal(0);
  });

  it('send data', (done) => {
    let rel = '';
    window.location.reload = () => {
      rel = 'reload';
    };

    wrapper.vm.createProposal();
    moxios.stubRequest(proxyLink('/api/v1/proposals/new'), {
      status: 200,
      response: {
        prop,
      },
    });

    moxios.wait(() => {
      expect(rel).to.equal('reload');
      done();
    });
  });
});

describe('ProposalsShow.vue', () => {
  beforeEach(() => {
    moxios.install();
  });

  afterEach(() => {
    moxios.uninstall();
  });

  const prop = {
    id: 1,
    createdAt: (new Date()).toISOString(),
    proposalType: 'SECURITY',
    status: 'ACCEPTED',
    quantity: 20,
    place: 'Place',
    security: {
      username: 'security',
    },
    description: 'Description',
  };

  const wrapper = shallowMount(ProposalsShow, {
    mocks: {
      $store: store,
    },
    localVue,
    propsData: {
      proposal: prop,
    },
  });

  it('renders input props', () => {
    expect(wrapper.props().proposal.id).to.equal(1);
    expect(wrapper.props().proposal.place).to.equal('Place');
    expect(wrapper.html()).to.include(`<td class="table__data">${prop.place}</td>`);
  });

  it('renders the correct markup', () => {
    expect(wrapper.contains('.ord__header')).to.be.true;
    expect(wrapper.contains('.ord__content')).to.be.true;
    expect(wrapper.contains('.ord__footer')).to.be.true;
    expect(wrapper.html()).to.include('<div class="submit">Отменить</div>');
    expect(wrapper.html()).to.include('<div class="submit submit__delete">Завершить</div>');
  });

  it('complete proposal', (done) => {
    let rel = '';
    window.location.reload = () => {
      rel = 'reload';
    };

    prop.status = 'COMPLETED';
    wrapper.vm.change('COMPLETED');
    const query = '/api/v1/proposals/1/change?status=COMPLETED';
    moxios.stubRequest(proxyLink(query), {
      status: 200,
      response: {
        prop,
      },
    });

    moxios.wait(() => {
      expect(rel).to.equal('reload');
      done();
    });
  });
});
