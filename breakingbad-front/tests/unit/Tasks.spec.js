import { shallowMount } from '@vue/test-utils';
import { expect } from 'chai';
import TasksShow from '@/components/tasks/TasksShow.vue';

import store from '../../src/store';

describe('TasksShow.vue', () => {
  const prop = {
    id: 1,
    createdAt: (new Date()).toISOString(),
    proposalType: 'SECURITY',
    status: 'ACCEPTED',
    place: 'Place',
    user: {
      username: 'security',
    },
    description: 'Description',
  };

  const wrapper = shallowMount(TasksShow, {
    mocks: {
      $store: store,
    },
    propsData: {
      proposal: prop,
    },
  });

  it('renders input props', () => {
    expect(wrapper.props().proposal.id).to.equal(1);
    expect(wrapper.props().proposal.place).to.equal('Place');
    expect(wrapper.props().proposal.description).to.equal('Description');
    expect(wrapper.html()).to.include(`<td class="table__data">${prop.place}</td>`);
    expect(wrapper.html()).to.include(`<td class="table__data">${prop.user.username}</td>`);
  });

  it('renders the correct markup', () => {
    expect(wrapper.contains('.ord__header')).to.be.true;
    expect(wrapper.contains('.ord__content')).to.be.true;
    expect(wrapper.contains('.ord__footer')).to.be.false;
    expect(wrapper.html()).to.include('<td colspan="3" class="table__data">Description</td>');
  });
});
