import moxios from 'moxios';
import Axios from 'axios';

import { shallowMount, createLocalVue } from '@vue/test-utils';
import { expect } from 'chai';
import Stats from '@/views/Statistic.vue';

import store from '../../src/store';

const localVue = createLocalVue();
localVue.use(Axios);
localVue.prototype.$http = Axios;

describe('Stats.vue', () => {
  beforeEach(() => {
    moxios.install();
  });

  afterEach(() => {
    moxios.uninstall();
  });

  const wrapper = shallowMount(Stats, {
    mocks: {
      $store: store,
    },
    localVue,
  });

  const stats = {
    startAt: (new Date()).toISOString(),
    endAt: (new Date()).toISOString(),
  };

  wrapper.setData({ statistic: stats });

  it('renders the correct markup', () => {
    expect(wrapper.html()).to.include('<form action="/api/v1/statistics/" class="form">');
    expect(wrapper.html()).to.include('<form action="/api/v1/statistics/" class="form">');
    expect(wrapper.html()).to.include('<h1 class="h1">Нет информации за указанный промежуток времени!</h1>');
  });

  it('has a button', () => {
    expect(wrapper.contains('.submit')).to.be.true;
  });

  it('set and check data', () => {
    expect(wrapper.vm.mode).to.equal('TABLE');
    expect(wrapper.vm.err).to.equal('');
    expect(wrapper.vm.info.length).to.equal(0);
  });
});
