import moxios from 'moxios';
import Axios from 'axios';

import { shallowMount, createLocalVue } from '@vue/test-utils';
import { expect } from 'chai';
import OrdersNew from '@/components/orders/OrdersNew.vue';
import OrdersShow from '@/components/orders/OrdersShow.vue';

import store from '../../src/store';
import { proxyLink } from '../../src/utils';

const localVue = createLocalVue();
localVue.use(Axios);
localVue.prototype.$http = Axios;

describe('OrdersNew.vue', () => {
  beforeEach(() => {
    moxios.install();
  });

  afterEach(() => {
    moxios.uninstall();
  });

  const wrapper = shallowMount(OrdersNew, {
    mocks: {
      $store: store,
    },
    localVue,
  });

  it('renders the correct markup', () => {
    expect(wrapper.html()).to.include('<form action="/api/v1/orders/new" class="form">');
  });

  it('has a button', () => {
    expect(wrapper.contains('.submit')).to.be.true;
  });

  it('set and check data', () => {
    const ord = {
      quantity: -20,
      city: 'city',
      district: 'district',
    };

    wrapper.setData({ order: ord });
    expect(wrapper.find('#quantity').element.value).to.equal('-20');
  });

  it('send data', (done) => {
    const ord = {
      quantity: -20,
      city: 'city',
      district: 'district',
    };

    wrapper.setData({ order: ord });
    wrapper.vm.createOrder();
    moxios.stubRequest(proxyLink('/api/v1/orders/new'), {
      status: 200,
      response: [{
        code: 'Negative.order.quantity',
      }],
    });

    moxios.wait(() => {
      expect(wrapper.vm.err).to.equal('Количество товара не может быть меньше нуля');
      done();
    });
  });
});

describe('OrdersShow.vue', () => {
  beforeEach(() => {
    moxios.install();
  });

  afterEach(() => {
    moxios.uninstall();
  });

  const ord = {
    id: 1,
    createdAt: (new Date()).toISOString(),
    quantity: 20,
    status: 'NEW',
    paymentStatus: 'UNPAID',
    amount: 20 * 20,
    city: 'city',
    district: 'district',
    dealer: null,
    deliveryAt: null,
    place: null,
    photo: null,
  };

  const wrapper = shallowMount(OrdersShow, {
    mocks: {
      $store: store,
    },
    localVue,
    propsData: {
      order: ord,
    },
  });

  it('renders input props', () => {
    expect(wrapper.props().order.id).to.equal(1);
    expect(wrapper.props().order.district).to.equal('district');
  });

  it('renders the correct markup', () => {
    expect(wrapper.contains('.ord__header')).to.be.true;
    expect(wrapper.contains('.ord__content')).to.be.true;
    expect(wrapper.contains('.ord__footer')).to.be.true;
    expect(wrapper.html()).to.include('<div class="submit">Отменить</div>');
  });

  it('cancel order', (done) => {
    let rel = '';
    window.location.reload = () => {
      rel = 'reload';
    };

    ord.status = 'CANCELED';
    wrapper.vm.change('CANCELED');
    const query = '/api/v1/orders/1/pay?status=CANCELED';
    moxios.stubRequest(proxyLink(query), {
      status: 200,
      response: {
        ord,
      },
    });

    moxios.wait(() => {
      expect(rel).to.equal('reload');
      done();
    });
  });
});
