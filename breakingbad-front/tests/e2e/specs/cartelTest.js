// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {
  'login action with bad credentials': (browser) => {
    browser
      .url(process.env.VUE_DEV_SERVER_URL)
      .waitForElementVisible('#app', 5000)
      .assert.elementPresent('.main__form')
      .assert.containsText('h1', 'Вход')
      .assert.elementCount('video', 1)
      .setValue('.input__login', 'gustavo')
      .setValue('.input__password', 'pwd')
      .waitForElementVisible('.submit', 1000)
      .pause(3000)
      .click('.submit')
      .pause(3000)
      .assert.containsText('.error', 'Неверная пара логин-пароль');
  },
  'login cartel': (browser) => {
    browser
      .clearValue('.input__login')
      .setValue('.input__login', 'gustavo')
      .clearValue('.input__password')
      .setValue('.input__password', 'gustavo')
      .waitForElementVisible('.submit', 1000)
      .pause(3000)
      .click('.submit')
      .pause(3000)
      .url((result) => {
        const reg = /.+?\:\/\/.+?(\/.+?)(?:#|\?|$)/;
        const pathname = reg.exec(result.value)[1];
        browser.assert.equal(pathname, '/messages');
      });
  },
  'cartel access to prohibited resources': (browser) => {
    browser
      .url(`${process.env.VUE_DEV_SERVER_URL}reports`)
      .pause(3000)
      .url((result) => {
        const reg = /.+?\:\/\/.+?(\/.+?)(?:#|\?|$)/;
        const pathname = reg.exec(result.value)[1];
        browser.assert.equal(pathname, '/messages');
      })
      .url(`${process.env.VUE_DEV_SERVER_URL}tasks`)
      .pause(3000)
      .url((result) => {
        const reg = /.+?\:\/\/.+?(\/.+?)(?:#|\?|$)/;
        const pathname = reg.exec(result.value)[1];
        browser.assert.equal(pathname, '/messages');
      });
  },
  'cartel check messages': (browser) => {
    browser
	   .setValue('.input', 'gust')
      .setValue('.textarea', 'hello')
      .click('.submit')
      .pause(1000)
      .assert.containsText('.error', 'Пользователь не найден');
  },
  'cartel check orders': (browser) => {
    browser
      .url(`${process.env.VUE_DEV_SERVER_URL}orders`)
      .pause(3000)
      .assert.elementCount('.orders-c__menu-item', 5)
      .assert.elementPresent('.codes__content');
  },
  'cartel check codes': (browser) => {
    browser
      .url(`${process.env.VUE_DEV_SERVER_URL}codes`)
      .pause(3000)
      .assert.elementCount('.form__group', 3)
      .assert.elementPresent('.submit')
      .assert.elementPresent('.codes__table');
  },
  'cartel check proposals': (browser) => {
    browser
      .url(`${process.env.VUE_DEV_SERVER_URL}proposals`)
      .pause(3000)
      .assert.elementCount('.orders-c__menu-item', 4)
      .assert.elementPresent('.codes__content');
  },
  'cartel check stats': (browser) => {
    browser
      .url(`${process.env.VUE_DEV_SERVER_URL}statistic`)
      .pause(3000)
      .assert.elementCount('.form__group', 3)
      .assert.elementPresent('.stats__content')
      .assert.elementCount('.stats__total', 2)
      .assert.elementPresent('.submit')
      .setValue('#startAt', '06.06.2019')
      .click('.submit')
      .pause(2000)
      .assert.containsText('.error', 'Неверный диапазон дат')
      .pause(1000)
      .setValue('#startAt', '06.06.2018')
      .click('.submit')
      .pause(2000)
      .assert.containsText('.error', '');
  },
  'logout action': (browser) => {
    browser
      .click('.link.link__logout')
      .pause(2000)
      .assert.containsText('h1', 'Вход')
      .end();
  },
};
