// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {
  'login security': (browser) => {
    browser
      .url(process.env.VUE_DEV_SERVER_URL)
      .waitForElementVisible('#app', 5000)
      .assert.elementPresent('.main__form')
      .setValue('.input__login', 'security')
      .setValue('.input__password', 'security')
      .waitForElementVisible('.submit', 1000)
      .pause(3000)
      .click('.submit')
      .pause(3000)
      .url((result) => {
        const reg = /.+?\:\/\/.+?(\/.+?)(?:#|\?|$)/;
        const pathname = reg.exec(result.value)[1];
        browser.assert.equal(pathname, '/messages');
      });
  },
  'security access to prohibited resources': (browser) => {
    browser
      .url(`${process.env.VUE_DEV_SERVER_URL}reports`)
      .pause(3000)
      .url((result) => {
        const reg = /.+?\:\/\/.+?(\/.+?)(?:#|\?|$)/;
        const pathname = reg.exec(result.value)[1];
        browser.assert.equal(pathname, '/messages');
      })
      .url(`${process.env.VUE_DEV_SERVER_URL}stats`)
      .pause(3000)
      .url((result) => {
        const reg = /.+?\:\/\/.+?(\/.+?)(?:#|\?|$)/;
        const pathname = reg.exec(result.value)[1];
        browser.assert.equal(pathname, '/messages');
      });
  },
  'security check messages': (browser) => {
    browser
      .setValue('.input', 'gust')
      .setValue('.textarea', 'hello')
      .click('.submit')
      .pause(1000)
      .assert.containsText('.error', 'Пользователь не найден');
  },
  'security check proposals': (browser) => {
    browser
      .url(`${process.env.VUE_DEV_SERVER_URL}proposals`)
      .pause(3000)
      .assert.elementCount('.orders-o__menu-icon', 5)
      .assert.elementCount('.form__group', 4)
      .setValue('#description', 'Описание')
      .setValue('.input', 'место')
      .click('.submit')
      .pause(2000)
      .assert.containsText('.error', '')
      .assert.elementPresent('.order');
  },
  'security check tasks': (browser) => {
    browser
      .url(`${process.env.VUE_DEV_SERVER_URL}tasks`)
      .pause(3000)
      .assert.elementCount('.orders-o__menu-icon', 3)
      .assert.elementPresent('.order')
      .click('.order')
      .pause(1000)
      .assert.elementPresent('.ord')
      .assert.elementCount('.ord__content', 1);
  },
  'logout action': (browser) => {
    browser
      .click('.link.link__logout')
      .pause(2000)
      .assert.containsText('h1', 'Вход')
      .end();
  },
};
