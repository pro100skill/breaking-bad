// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {
  'login provider': (browser) => {
    browser
      .url(process.env.VUE_DEV_SERVER_URL)
      .waitForElementVisible('#app', 5000)
      .assert.elementPresent('.main__form')
      .setValue('.input__login', 'provider')
      .setValue('.input__password', 'provider')
      .waitForElementVisible('.submit', 1000)
      .pause(3000)
      .click('.submit')
      .pause(3000)
      .url((result) => {
        const reg = /.+?\:\/\/.+?(\/.+?)(?:#|\?|$)/;
        const pathname = reg.exec(result.value)[1];
        browser.assert.equal(pathname, '/messages');
      });
  },
  'provider access to prohibited resources': (browser) => {
    browser
      .url(`${process.env.VUE_DEV_SERVER_URL}orders`)
      .pause(3000)
      .url((result) => {
        const reg = /.+?\:\/\/.+?(\/.+?)(?:#|\?|$)/;
        const pathname = reg.exec(result.value)[1];
        browser.assert.equal(pathname, '/messages');
      })
      .url(`${process.env.VUE_DEV_SERVER_URL}codes`)
      .pause(3000)
      .url((result) => {
        const reg = /.+?\:\/\/.+?(\/.+?)(?:#|\?|$)/;
        const pathname = reg.exec(result.value)[1];
        browser.assert.equal(pathname, '/messages');
      });
  },
  'provider check messages': (browser) => {
    browser
      .assert.containsText('h1', 'Нет сообщений')
      .setValue('.input', 'provider')
      .setValue('.textarea', 'hello')
      .click('.submit')
      .pause(1000)
      .assert.elementPresent('.message')
      .assert.containsText('.error', '')
      .click('.messages__icon_out')
      .pause(1000)
      .assert.elementPresent('.message');
  },
  'provider check proposals': (browser) => {
    browser
      .url(`${process.env.VUE_DEV_SERVER_URL}proposals`)
      .pause(3000)
      .assert.elementCount('.orders-o__menu-icon', 5)
      .assert.containsText('h1', 'Создать заявку')
      .click('#proposalType option[value=CASH]')
      .click('.submit__add')
      .pause(1000)
      .setValue('.input', 'ingredient')
      .setValue('.input__number', '-33')
      .click('.submit__all')
      .pause(1000)
      .assert.containsText('.error', 'Цена ингредиента не может быть меньше нуля');
  },
  'provider check reports': (browser) => {
    browser
      .url(`${process.env.VUE_DEV_SERVER_URL}reports`)
      .pause(3000)
      .assert.elementPresent('.ingredients')
      .assert.containsText('h1', 'Создать отчет')
      .setValue('.input', 'ingredient')
      .setValue('.input__number', '-3')
      .click('.submit__report')
      .pause(2000)
      .assert.containsText('.error', 'Итого не может быть меньше нуля');
  },
  'logout action': (browser) => {
    browser
      .click('.link.link__logout')
      .pause(2000)
      .assert.containsText('h1', 'Вход')
      .end();
  },
};
