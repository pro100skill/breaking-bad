// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {
  'login dealer': (browser) => {
    browser
      .url(process.env.VUE_DEV_SERVER_URL)
      .waitForElementVisible('#app', 5000)
      .assert.elementPresent('.main__form')
      .setValue('.input__login', 'dealer')
      .setValue('.input__password', 'dealer')
      .waitForElementVisible('.submit', 1000)
      .pause(3000)
      .click('.submit')
      .pause(3000)
      .url((result) => {
        const reg = /.+?\:\/\/.+?(\/.+?)(?:#|\?|$)/;
        const pathname = reg.exec(result.value)[1];
        browser.assert.equal(pathname, '/messages');
      });
  },
  'dealer access to prohibited resources': (browser) => {
    browser
      .url(`${process.env.VUE_DEV_SERVER_URL}reports`)
      .pause(3000)
      .url((result) => {
        const reg = /.+?\:\/\/.+?(\/.+?)(?:#|\?|$)/;
        const pathname = reg.exec(result.value)[1];
        browser.assert.equal(pathname, '/messages');
      })
      .url(`${process.env.VUE_DEV_SERVER_URL}tasks`)
      .pause(3000)
      .url((result) => {
        const reg = /.+?\:\/\/.+?(\/.+?)(?:#|\?|$)/;
        const pathname = reg.exec(result.value)[1];
        browser.assert.equal(pathname, '/messages');
      });
  },
  'dealer check messages': (browser) => {
    browser
      .setValue('.input', 'gust')
      .setValue('.textarea', 'hello')
      .click('.submit')
      .pause(1000)
      .assert.containsText('.error', 'Пользователь не найден');
  },
  'dealer check proposals': (browser) => {
    browser
      .url(`${process.env.VUE_DEV_SERVER_URL}proposals`)
      .pause(3000)
      .assert.elementCount('.orders-o__menu-icon', 5)
      .assert.containsText('h1', 'Создать заявку')
      .click('#proposalType option[value=GOOD]')
      .setValue('.input__place', 'place')
      .setValue('.input__count', -33)
      .click('.submit__all')
      .pause(1000)
      .assert.containsText('.error', 'Количество товара не может быть меньше нуля');
  },
  'dealer check orders': (browser) => {
    browser
      .url(`${process.env.VUE_DEV_SERVER_URL}orders`)
      .pause(3000)
      .assert.elementCount('.orders-o__menu-icon', 4)
      .assert.containsText('h1', 'Нет заказов')
      .click('.orders-o__menu-icon_exec')
      .pause(1000)
      .click('.order')
      .pause(1000)
      .assert.elementPresent('.ord')
      .assert.elementPresent('.ord__photo')
      .assert.elementCount('.ord__content', 1);
  },
  'logout action': (browser) => {
    browser
      .click('.link.link__logout')
      .pause(2000)
      .assert.containsText('h1', 'Вход')
      .end();
  },
};
