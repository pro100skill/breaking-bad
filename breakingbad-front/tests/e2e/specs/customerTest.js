// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {
  'login customer': (browser) => {
    browser
      .url(process.env.VUE_DEV_SERVER_URL)
      .waitForElementVisible('#app', 5000)
      .assert.elementPresent('.main__form')
      .setValue('.input__login', 'customer')
      .setValue('.input__password', 'customer')
      .waitForElementVisible('.submit', 1000)
      .pause(3000)
      .click('.submit')
      .pause(3000)
      .url((result) => {
        const reg = /.+?\:\/\/.+?(\/.+?)(?:#|\?|$)/;
        const pathname = reg.exec(result.value)[1];
        browser.assert.equal(pathname, '/messages');
      });
  },
  'customer access to prohibited resources': (browser) => {
    browser
      .url(`${process.env.VUE_DEV_SERVER_URL}proposals`)
      .pause(3000)
      .url((result) => {
        const reg = /.+?\:\/\/.+?(\/.+?)(?:#|\?|$)/;
        const pathname = reg.exec(result.value)[1];
        browser.assert.equal(pathname, '/messages');
      })
      .url(`${process.env.VUE_DEV_SERVER_URL}codes`)
      .pause(3000)
      .url((result) => {
        const reg = /.+?\:\/\/.+?(\/.+?)(?:#|\?|$)/;
        const pathname = reg.exec(result.value)[1];
        browser.assert.equal(pathname, '/messages');
      });
  },
  'customer check messages': (browser) => {
    browser
      .click('.messages__support')
      .pause(1000)
      .setValue('.textarea', 'hello')
      .click('.submit')
      .pause(1000)
      .assert.containsText('.error', '')
      .click('.messages__icon_out')
      .pause(1000)
      .click('.message')
      .assert.elementPresent('.msg__header')
      .assert.elementPresent('.msg__content')
      .assert.elementPresent('.msg__footer');
  },
  'customer check orders': (browser) => {
    const price = 20;
    const count = -20;
    browser
      .url(`${process.env.VUE_DEV_SERVER_URL}orders`)
      .pause(3000)
      .assert.elementCount('.orders-o__menu-icon', 6)
      .assert.containsText('h1', 'Заказать товар')
      .setValue('.input__count', count)
      .setValue('.input__city', 'city')
      .setValue('.input__district', 'district')
      .assert.containsText('.ord__summary', `$ ${price * count}`)
      .click('.submit')
      .pause(1000)
      .assert.containsText('.error', 'Количество товара не может быть меньше нуля');
  },
  'logout action': (browser) => {
    browser
      .click('.link.link__logout')
      .pause(2000)
      .assert.containsText('h1', 'Вход')
      .end();
  },
};
