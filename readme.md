# Breaking Bad Project for MPI

Этот проект выполнен в рамках предмета МПИ, за источник сбора требований к системе взят сериал ["Breaking Bad"](https://en.wikipedia.org/wiki/Breaking_Bad).

Используемые фреймворки Spring Boot + Vue-Cli

## How to install and use

### Backend
```
1. Переименовать application.properties.example в application.properties
2. Изменить логин и пароль для подключения к БД в application.properties
3. Запустить файл up.sql
4. Установить зависимости Maven
5. Запустить BreakingbadApplication.java
```

### Frontend
```
1. Установить зависимости npm i
2. Создать файл .env в корневой директории со строкой VUE_APP_DEBUG=true
3. Запустить npm run serve
```